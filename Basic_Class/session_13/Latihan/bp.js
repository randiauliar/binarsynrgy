var http = require('http');
var fs = require('fs');
const port = 8080;
http.createServer(function (req, res) {
  fs.readFile('disp.html', function(err, data) {
    console.log('service active at port'+port);
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    return res.end();
  });
}).listen(port);
