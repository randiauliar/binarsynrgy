const express = require('express');
const MongoClient = require('mongodb').MongoClient
const bodyParser= require('body-parser')
var ObjectID = require('mongodb').ObjectID;
var url = "mongodb://localhost:27017/";
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

MongoClient.connect(url, (err, db) => {
  if (err) return console.log(err)

  app.listen(3000, () => {
    console.log('app working on 3000')
  });

  let dbase = db.db("cities");

  app.get('/cities', (req, res, next) => {
    dbase.collection('cities').find({}, {
        projection: {_id:0, city: 1, country: 1}}).toArray( (err, results) => {
      res.json(results)
    });
  });
  app.get('/city', (req, res, next) => {
    dbase.collection("cities").aggregate([
      { $sample: { size: 1 }}]).toArray( (err, results) => {
      res.json(results)
    });
  });


});