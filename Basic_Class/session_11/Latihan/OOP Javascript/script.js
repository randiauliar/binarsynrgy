//definisi class
class eMoneyAccount {
  //definisi constructor
  //asumsi: user mendaftar dengan nama & telp, dan saldo awal 0
  constructor(nameInit, phoneInit) {
    this.name = nameInit;
    this.phone = phoneInit;
    this.amount = 0;
  }

  //ketika topup, saldo akan bertambah sesuai jumlah yang di topup
  topUp(value) {
    this.amount += value;
  }

  //membelanjakan sejumlah value
  spend(value) {
    this.amount -= value;
  }

  //recipient adalah object eMoney Account lain
  //value adalah jumlah transfer
  transfer(recipient, value) {
    recipient.amount += value;
    this.amount -= value;
  }

  //setter atribut nama dan no hp
  setname(newName) {
    this.name = newName;
  }

  setPhone(newPhone) {
    this.phone = newPhone;
  }
}

//instansiasi: membentuk objek riil dari class
var Dwi = new eMoneyAccount("Dwi","0808080");
console.log("saldo Dwi: "+Dwi.amount);
var Abrar = new eMoneyAccount("Abrar","12345");
console.log("saldo Abrar: "+Abrar.amount);
console.log("==========");
console.log("Dwi top up Rp300.000");
Dwi.topUp(300000);
console.log("saldo Dwi: "+Dwi.amount);
console.log("==========");
console.log("Dwi transfer Rp80.000 ke Abrar");
Dwi.transfer(Abrar, 80000);
console.log("saldo Dwi: "+Dwi.amount);
console.log("saldo Abrar: "+Abrar.amount);

//tampilkan informasi akun emoney Dwi & Abrar
document.getElementById("namadwi").innerHTML = Dwi.name;
document.getElementById("hpdwi").innerHTML = Dwi.phone;
document.getElementById("amdwi").innerHTML = Dwi.amount;
document.getElementById("namaabrar").innerHTML = Abrar.name
document.getElementById("hpabrar").innerHTML = Abrar.phone;
document.getElementById("amabrar").innerHTML = Abrar.amount;