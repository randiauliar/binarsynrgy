//definisi class
class eMoneyAccount {
  //definisi constructor
  //asumsi: user mendaftar dengan nama & telp, dan saldo awal 0
  constructor(nameInit, phoneInit) {
    this.name = nameInit;
    this.phone = phoneInit;
    this.amount = 0;
  }

  //ketika topup, saldo akan bertambah sesuai jumlah yang di topup
  topUp(value) {
    //pastikan tipe data value adalah number
    this.amount += value;
  }

  //membelanjakan sejumlah value
  spend(value) {
    //pastikan tipe data value adalah number
    //apabila saldo pengguna < value, spend gagal

    this.amount -= value;
  }

  //recipient adalah object eMoney Account lain
  //value adalah jumlah transfer
  transfer(recipient, value) {
    //pastikan tipe data recipient adalah eMoneyAccount
    //pastikan tipe data value adalah number
    //apabila saldo pengguna < value, transfer gagal

    recipient.amount += value;
    this.amount -= value;
  }

  //setter atribut nama dan no hp
  setName(newName) {
    this.name = newName;
  }

  setPhone(newPhone) {
    this.phone = newPhone;
  }
}

//instansiasi: membentuk objek riil dari class
var Dwi = new eMoneyAccount("Dwi","0808080");
console.log("saldo Dwi: "+Dwi.amount);
var Abrar = new eMoneyAccount("Abrar","12345");
console.log("saldo Abrar: "+Abrar.amount);
console.log("==========");
console.log("Dwi top up Rp300.000");
Dwi.topUp(300000);
console.log("saldo Dwi: "+Dwi.amount);
console.log("==========");
console.log("Dwi transfer Rp80.000 ke Abrar");
Dwi.transfer(Abrar, "80000");//kesalahan, transfer string
console.log("saldo Dwi: "+Dwi.amount);
console.log("saldo Abrar: "+Abrar.amount);
console.log("==========");
console.log("Abrar top up Rp300.000");
Abrar.topUp(300000);
console.log("saldo Dwi: "+Dwi.amount);
console.log("saldo Abrar: "+Abrar.amount);

//tampilkan informasi akun emoney Dwi & Abrar

function fetchLyrics(artist, title) {
    const Http = new XMLHttpRequest();
    var url = 'https://api.lyrics.ovh/v1/'+artist+'/'+title;
    var output = document.getElementById("lyricoutput");
    Http.open("GET", url); //kirim HTTP request dengan metode GET
    Http.send();

    //ketika balasan dari server sudah ada
    Http.onreadystatechange = function() {
        if(this.readyState==4) {
        var result = JSON.parse(Http.responseText);
        output.innerHTML = result.lyrics;
        }
    }
}

