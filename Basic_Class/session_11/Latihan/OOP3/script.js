//definisi class
class eMoneyAccount {
    //definisi constructor
    //asumsi: user mendaftar dengan nama & telp, dan saldo awal 0
    constructor(nameInit, phoneInit) {
      this.name = nameInit;
      this.phone = phoneInit;
      this.amount = 0;
    }
  
    //ketika topup, saldo akan bertambah sesuai jumlah yang di topup
    topUp(value) {
      var value;
      if (typeof value === 'number'){
        this.amount += value;
      } else {
          console.log("TopUp Gagal");
      }
      }
  
    //membelanjakan sejumlah value
    spend(value) {
      var value;
      if (typeof value === 'number' && this.amount >= value)
      { 
        this.amount -= value;
      } else {
          console.log("Spend Gagal");
      }
    }
  
    //recipient adalah object eMoney Account lain
    //value adalah jumlah transfer
    transfer(recipient, value) {
      if (recipient === this.name){
        recipient.amount += value;
        this.amount -= value;
      } else 
       console.log("Tidak ada penerima")
    }
  
    //setter atribut nama dan no hp
    setname(newName) {
      this.name = newName;
    }
    setphone(newPhone) {
      this.name = newPhone;
    }
}

function topupsaldo() {
  var message, x;
  message = document.getElementById("p01");
  message.innerHTML = "";
  x = document.getElementById("saldo").value;
  try { 
    if(x == "")  throw "masukkan nominal topup!";
    if(isNaN(x)) throw "nominal salah";
	  if(typeof x === 'number') throw "berhasil topup";
  }
  catch(err) {
    message.innerHTML = err;
  }
}
function transferuang() {
  var message, x;
  message = document.getElementById("p01");
  message.innerHTML = "";
  x = document.getElementById("saldo").value;
  try { 
    if(x == "")  throw "masukkan nominal transfer!";
    if(isNaN(x)) throw "nominal salah";
	  if(typeof x === 'number') throw "berhasil transfer";
  }
  catch(err) {
    message.innerHTML = err;
  }
}
  //instansiasi: membentuk objek riil dari class
  var Dwi = new eMoneyAccount("Dwi","0808080");
  console.log("saldo Dwi: "+Dwi.amount);
  var Abrar = new eMoneyAccount("Abrar","12345");
  console.log("saldo Abrar: "+Abrar.amount);
  console.log("==========");
  console.log("Dwi top up Rp300.000");
  Dwi.topUp(300000);
  console.log("saldo Dwi: "+Dwi.amount);
  console.log("==========");
  console.log("Dwi pakai 200.000 buat BO");
  Dwi.spend(200000);
  console.log("saldo Dwi: "+Dwi.amount);
  console.log("==========");
  console.log("Dwi transfer Rp80.000 ke Abrar");
  Dwi.transfer(Abrar, 80000);
  console.log("saldo Dwi: "+Dwi.amount);
  console.log("saldo Abrar: "+Abrar.amount);


  // document.getElementById("dwi").innerHTML = "Nama: "+Dwi.name+"</br>"+"No. HP: "+Dwi.phone+"</br>"+"Saldo: " + Dwi.amount;
  // document.getElementById("abrar").innerHTML = "Nama: "+Abrar.name+"</br>"+"No. HP: "+Abrar.phone+"</br>"+"Saldo: " + Abrar.amount;
//tampilkan informasi akun emoney Dwi & Abrar  ("{}ImasAbrarDimasAbrar