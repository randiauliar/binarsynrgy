var itemCount = 0;
//add input fields for item: name, price, quantity, total price
function addItem(){
  var form = document.getElementById('checkout_form');
  //mencari elemen dokumen dengan id 'checkout_form'
  itemCount++; //increment, nambah 1

  //create input field
  var itemName = document.createElement("input");
  itemName.id = 'item_name_' + itemCount; //operator string, menggabungkan string
  itemName.className = 'form-control';
  itemName.type = 'text';
  itemName.name = 'item';
  itemName.placeholder = 'Name Item ' + itemCount;

  var itemPrice = document.createElement("input");
  itemPrice.id = 'item_price_' + itemCount;
  itemPrice.className = 'form-control';
  itemPrice.type = 'text';
  itemPrice.name = 'price';
  itemPrice.placeholder = 'Price';
  // itemPrice.value = 0;

  var itemQty = document.createElement("input");
  itemQty.id = 'item_qty_' + itemCount;
  itemQty.className = 'form-control';
  itemQty.type = 'text';
  itemQty.name = 'qty';
  itemQty.placeholder = 'Quantity';
  // itemQty.value = 0;

  var totalItemPrice = document.createElement("p");
  totalItemPrice.id = 'total_item_' + itemCount;
  totalItemPrice.value = 0;
  totalItemPrice.name = 'total_item';

  //create div wrapper, bootstrap style
  var col1 = document.createElement("div");
  col1.className = 'col';
  var col2 = document.createElement("div");
  col2.className = 'col';
  var col3 = document.createElement("div");
  col3.className = 'col';
  var col4 = document.createElement("div");
  col4.className = 'col';

  //create row
  var row = document.createElement("div");
  row.className = 'row py-2';

  //append order
  //compare with docs https://getbootstrap.com/docs/4.1/components/forms/
  form.appendChild(row);

  row.appendChild(col1);
  row.appendChild(col2);
  row.appendChild(col3);
  row.appendChild(col4);

  col1.appendChild(itemName);
  col2.appendChild(itemPrice);
  col3.appendChild(itemQty);
  col4.appendChild(totalItemPrice);
  // itemPrice.onkeyup = calcTotalItem(itemPrice);
  // itemQty.onkeyup = calcTotalItem(itemQty);

}

function calcTotalItem(obj) {
  console.log('calcTotalItem called'); //debugging
  var itemRow = getItemRow(obj); //getItemRow adalah fungsi
  console.log('itemRow = ' + itemRow);
  var e = obj.id.toString();
  console.log('e= '+e);
  if (e == 'item_qty_' + itemRow) {
      x = Number(obj.value); //casting string to number
      y = Number(document.getElementById('item_price_' + itemRow).value);
      
      console.log('x=' + x + ', y='+ y + ', x*y='+ x*y);
  } else {
      x = Number(document.getElementById('item_qty_' + itemRow).value);
      y = Number(obj.value);
      console.log('x=' + x + ', y='+ y + ', x*y='+ x*y);
  }
  document.getElementById('total_item_' + itemRow).innerHTML = Number (x * y);
}

function getItemRow(obj) {
  return obj.id.toString().slice(-1);//mengambil id elemen HTML, kemudian memilih karakter terakhirnya.
  //misal format id nama_item_2, maka diperoleh 2
}//

//target: bisa menampilkan total belanja semua item
//target tambahan: ada button untuk export sebagai JSON