//export model supaya bisa dipakai file lain
module.exports = mongoose => {
  //definisi schema, termasuk validasi
  var schema = mongoose.Schema(
    {
      name: String,
      description: String,
      published: Boolean
    },
    { timestamps: true }
  );

  //fungsi mejik. jangan disentuh
  schema.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  //schema dibangun sebagai model Tutorial, mengacu pada collection "tutorial" 
  const Tutorial = mongoose.model("tutorial", schema);
  return Tutorial;
};
