//export model supaya bisa dipakai file lain
module.exports = mongoose => {
  //definisi schema, termasuk validasi
  var schema = mongoose.Schema(
    {
      name: String,
      slug: String,
      is_active: Boolean
    },
    { timestamps: true }
  );

  //fungsi mejik. jangan disentuh
  schema.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  //schema dibangun sebagai model Category, mengacu pada collection "category" 
  const Category = mongoose.model("category", schema);
  return Category;
};
